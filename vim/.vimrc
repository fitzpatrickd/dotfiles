call plug#begin('~/.vim/plugged')
call plug#begin('~/.vim/plugged')

"Plug 'user/repo'
Plug 'scrooloose/nerdtree',
Plug 'kien/ctrlp.vim',
Plug 'Valloric/YouCompleteMe',
Plug 'jpo/vim-railscasts-theme',
Plug 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim'},
Plug 'scrooloose/nerdtree',
Plug 'Xuyuanp/nerdtree-git-plugin',
Plug 'majutsushi/tagbar',
Plug 'tpope/vim-fugitive',
Plug 'w0rp/ale',
"Plug 'cjrh/vim-conda',
call plug#end()

"General
set nocompatible
filetype indent plugin on
syntax on
set confirm
let mapleader=","
set hidden
set autochdir


"Asthetics
set visualbell
set t_Co=256
set term=screen-256color
set ttymouse=xterm2
colorscheme railscasts
set number
set nowrap
set mouse=a

"Tabs Spacing
set backspace=indent,eol,start
set showmatch
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix 

au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2


"Splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"YouCompeteMe
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1

"Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }

autocmd vimenter * NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
map <C-n> :NERDTreeToggle<CR>
let NERDTreeIgnore = ['\.pyc$']
let NERDTreeChDirMode=2
let g:NERDTreeShowGitStatus = 1
let g:NERDTreeUpdateOnWrite = 1

nmap <F8> :TagbarToggle<CR>
let g:airline#extensions#tabline#enabled = 1

let g:ycm_python_binary_path = 'python'
